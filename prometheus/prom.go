package prometheus

import (
	"context"
	"fmt"
	"github.com/prometheus/client_golang/api"
	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/model"
	"math"
	"os"
	"time"
)

var Url string

func connect() api.Client {

	client, err := api.NewClient(api.Config{
		Address: Url,
	})
	if err != nil {
		fmt.Printf("Error creating client: %v\n", err)
		os.Exit(1)
	}
	return client
}

func usage(metricQuery string, calc func(result model.Value) float64) float64 {

	client := connect()
	v1api := v1.NewAPI(client)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	r := v1.Range{
		Start: time.Now().Add(-time.Hour),
		End:   time.Now(),
		Step:  time.Minute,
	}

	result, warnings, err := v1api.QueryRange(ctx, metricQuery, r)
	if err != nil {
		fmt.Printf("Error querying Prometheus: %v\n", err)
		os.Exit(1)
	}
	if len(warnings) > 0 {
		fmt.Printf("Warnings: %v\n", warnings)
	}

	return calc(result)
}

func cpuCalc(result model.Value) float64 {
	val := result.(model.Matrix)[0].Values
	first, last := 0.0, 0.0
	firstTime, lastTime := math.MaxInt64, 0

	for _, v := range val {
		if int(v.Timestamp) > lastTime {
			lastTime = int(v.Timestamp)
			last = float64(v.Value)
		}

		if int(v.Timestamp) < firstTime {
			firstTime = int(v.Timestamp)
			first = float64(v.Value)
		}
	}

	return (last - first) / (60 * 60) // seconds in an hour
}

func memoryCalc(result model.Value) float64 {
	sum, cnt := 0.0, 0.0
	for _, val := range result.(model.Matrix)[0].Values {
		sum += float64(val.Value)
		cnt++
	}
	return sum / cnt
}

func CpuUsage(podName, containerName string) float64 {
	return usage(fmt.Sprintf(`container_cpu_usage_seconds_total{image!="",name=~"^k8s_.*",pod="%s",container="%s"}`, podName, containerName), cpuCalc)
}

func MemoryUsage(podName, containerName string) float64 {
	return usage(fmt.Sprintf(`container_memory_usage_bytes{image!="",name=~"^k8s_.*",pod="%s",container="%s"}`, podName, containerName), memoryCalc)
}
