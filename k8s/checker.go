package k8s

import (
	"context"
	"fmt"
	"gitlab.com/blintmester/k8s-checker/prometheus"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func Check(clientset *kubernetes.Clientset) {

	cpuEpsilon := 0.2
	memoryEpsilon := 0.2

	pods, err := clientset.CoreV1().Pods("").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err.Error())
	}

	for _, pod := range pods.Items {
		errorMessage := ""

		if pod.Spec.PriorityClassName == "" {
			errorMessage += "pod does not have a priority class\n"
		}
		containers := &pod.Spec.Containers

		for _, container := range *containers {
			// k8s if ReadinessProbe exists
			if container.ReadinessProbe == nil {
				errorMessage += container.Name + " container has no readiness probe\n"
			}

			// k8s if LivenessProbe exists
			if container.LivenessProbe == nil {
				errorMessage += container.Name + " container has no liveness probe\n"
			}

			// k8s if resource limits exists
			if container.Resources.Limits == nil {
				errorMessage += container.Name + " container has no resources limits\n"
			}

			// k8s if resource requests exists
			if container.Resources.Requests == nil {
				errorMessage += container.Name + " container has no resources requests\n"
			} else {
				cpuReq := container.Resources.Requests.Cpu().AsApproximateFloat64()
				cpuUsed := prometheus.CpuUsage(pod.Name, container.Name)

				errorMessage += requestsChecker(cpuReq, cpuUsed, cpuEpsilon, "cpu", container.Name)

				memoryReq := container.Resources.Requests.Memory().AsApproximateFloat64()
				memoryUsed := prometheus.MemoryUsage(pod.Name, container.Name)

				errorMessage += requestsChecker(memoryReq, memoryUsed, memoryEpsilon, "memory", container.Name)
			}

		}
		if errorMessage != "" {
			fmt.Printf("❌  %s:\n", pod.Name)
			fmt.Println(errorMessage)
		}
	}
}

func requestsChecker(req, used, epsilon float64, resourceName, containerName string) string {
	if req == 0 {
		return fmt.Sprintf("%s container has no %s requests\n", containerName, resourceName)
	}

	/*if used == 0 {
		return fmt.Sprintf("%s container has no %s usage\n", containerName, resourceName)
	}*/

	if used*1.1*(1+epsilon) < req || used > req {
		return fmt.Sprintf("%s container's %s usage is %.1f%% of requests 😱\n", containerName, resourceName, used/req*100)
	}
	return ""
}
