package main

import (
	"gitlab.com/blintmester/k8s-checker/k8s"
)

func main() {
	k8s.Check(k8s.Connect())
}
