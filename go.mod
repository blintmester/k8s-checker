module gitlab.com/blintmester/k8s-checker

go 1.16

require (
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/common v0.26.0
	gitlab.com/MikeTTh/env v0.0.0-20210102155928-2e9be3823cc7
	k8s.io/apimachinery v0.21.3
	k8s.io/client-go v0.21.3
)
